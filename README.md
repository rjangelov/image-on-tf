# DevOps Task

##Tasks list
- [x] [1] Create CI/CD pipeline in one of the cloud providers on your choice (Github, Gitlab, Azure Devops. Etc ..), which builds the java code that is in that repo
- [x] [2] Your pipeline should build a Docker image that contains the builded java app and push it in a Docker registry of your choice.
- [x] [3] Using Terraform create infrastructure to host the Docker image that you created in the previous step in one of the cloud providers of your choice.
- [x] [4] Extend your pipeline and add steps to deploy your Docker image to the infrastructure that you created with Terraform.
- [x] [5] Make some changes to the java code – Change the greetings message that is displayed from Tomcat - from "Greetings from Spring Boot!" to "Greetings from DevOps team!"
- [x] [6] Deploy your code with the changes that you added.
- [x] [7] Upload the whole code of your solution in Github repository and send link to that repo and link to the hosted application, so the DevOps team can review your work.




##Notes:
- First run is meant to fail because of the dynamic key provisioning
- The backend used is TF Cloud
- TF provisioning is also added to the pipeline

##Useful commands:
- terraform output -raw instance_ip
- terraform output -raw private_key > deployer-key
- chmod 600 deployer-key
- ssh ec2-user@<IP-ADDRESS> -i deployer-key
