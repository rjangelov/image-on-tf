terraform {
  cloud {
    organization = "test_rado"

    workspaces {
      name = "rado-devops"
    }
  }
}


resource "aws_instance" "example" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  # Other necessary configuration...
  # Associate the security group
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]

  # Associate the key pair
  key_name = aws_key_pair.deployer_key.key_name
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic and 8080"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_key_pair" "deployer_key" {
  key_name   = "ec2-deployer-key"
  public_key = tls_private_key.example.public_key_openssh
}

resource "aws_eip" "app" {
  instance = aws_instance.example.id
}

resource "tls_private_key" "example" {
  algorithm = "RSA"
  rsa_bits  = 2048
}


